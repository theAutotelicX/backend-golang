package app

import (
	"git/gox/app/controllers"
)

var server = controllers.Server{}

func Run() {
	server.Initalize()

	server.Run()
}
