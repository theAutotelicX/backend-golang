package controllers

import (
	"git/gox/app/services"
	"github.com/gofiber/fiber/v2"
)

func (server *Server) SetupRoutes() {

	server.App.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hii")
	})

	server.App.Get("/task-request", func(c *fiber.Ctx) error {
		services.GetTask()
		return c.SendString("Hello, Tasks!")
	})
}
