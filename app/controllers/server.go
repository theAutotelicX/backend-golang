package controllers

import "github.com/gofiber/fiber/v2"

type Server struct {
	App  *fiber.App
}

func(server *Server) Initalize() {
	server.App = fiber.New()
}

func(server *Server) Run() {
	server.SetupRoutes()
	server.App.Listen(":8085")
}